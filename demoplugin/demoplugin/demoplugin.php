<?php

/**
 * @package DEMOPLUGIN
 */
/*
Plugin Name: demoplugin
Plugin URI: https://localhost/demoplugin
Description: description about demoplugin.
Version: 1.4
Author: Demoplugin
Author URI: https://localhost/demoplugin/author
License: GPLv2 or later
Text Domain: DEMOPLUGIN
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2005-2015 Demoplugin, Inc.
*/

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define('DEMOPLUGIN_VERSION', '1.4');
define('DEMOPLUGIN__MINIMUM_WP_VERSION', '4.0');
define('DEMOPLUGIN__PLUGIN_DIR', plugin_dir_path(__FILE__));
define('DEMOPLUGIN_DELETE_LIMIT', 100000);

register_activation_hook(__FILE__, array('DEMOPLUGIN', 'plugin_activation'));
register_deactivation_hook(__FILE__, array('DEMOPLUGIN', 'plugin_deactivation'));


require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'http://localhost/demoplugin/details.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'demoplugin'
);
